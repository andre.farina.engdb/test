# Zeebe Worker

### Creating worker for dev
  
- Clone all digiligs repos

- Docker image with all digilibs java maven deps:
```
$ docker pulll gcr.io/tim-pmid-dev/mvn-deps
$ # if you don't have gcp registry access, clone all digilibs repos
$ cd mvn-deps
$ ./build.sh
```

- Build Worker with Dockerfile
ex:
```
$ cd ../../db-z-worker
$ docker build -t db-z --file ../ci-cd/worker-z/Dockerfile-parents .
```
- Run docker with volume at /config with spring-boot resources files
ex:
```
$ cd ../../db-z-worker-sample
$ docker run -v $(pwd)/dev:/config db-z
```
- Or create a image with  spring-boot resources files files:
ex:
```
$ cd ../../db-z-worker-sample
$ docker build db-sample --build-arg RESOURCES_PATH=dev --build-arg WORKER_IMAGE=db-z -t -f ../ci-cd/worker-z/Dockerfile .
$ docker run db-sample
```

### Testing helm chart
```
helm install --namespace dev \
--name=db-z-worker-sample \
--set version=ma-1234 \
--set image.repository=gcr.io/tim-pmid-dev/db-z-worker \
--set image.tag=ma-5e806 \
--debug --dry-run \
.
```