{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "wz.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "wz.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common selector
*/}}
{{- define "wz.selectors" -}}
app.kubernetes.io/name: {{ include "wz.name" . }}
app.kubernetes.io/version: {{ (split "/" .Values.image.repository)._2 }}-{{ .Values.image.tag }} 
app.kubernetes.io/runtime: {{ .Values.runtime }}
app.kubernetes.io/instance: {{ .Values.name }}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "wz.labels" -}}
{{ include "wz.selectors" . }}
helm.sh/chart: {{ include "wz.chart" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}
