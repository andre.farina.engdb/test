# Usage

Install dependencies.

`pip install -r requirements`

Now run app.py, replacing projectName with your project's name and ioneDirectory with the absolute path for the ione folder on your machine, like the example below

`python app.py `**`<projectName> <ioneDirectory>`**

## Example

`python app.py "http-z-imdb" "/home/eng/ione"`