import os
import argparse
from mako.template import Template
from lxml import etree

parser = argparse.ArgumentParser(description='Join all mocks in one')
parser.add_argument('name', type=str, help='Name of project. Example: http-z-imdb')
parser.add_argument('ioneDir', type=str, help='Base directory of ione. Example: /home/eng/ione')
parser.add_argument('--projectId', type=str, help='Id of project, auto-generated previously by SoapUI')
parser.add_argument('--restMockServiceId', type=str, help='Id of each mock service, auto-generated previously by SoapUI')
parser.add_argument('--absDir', type=str, help='if ioneDir arg directory is absolute to mocks')
args = parser.parse_args()

projectDir = args.ioneDir+'/'+args.name+'/'
mocksDir = projectDir+'soapui-mock/mocks'
if args.absDir == "true":
    projectDir = args.ioneDir+"/../../"
    mocksDir = args.ioneDir

def getRestMockActionBigString():
    nsmap = {'con': 'http://eviware.com/soapui/config', 'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}

    restMockActionList = ""

    for filename in os.listdir(mocksDir):
        if not filename.endswith('.xml'): continue
        fullname = os.path.join(mocksDir, filename)
        data = etree.parse(fullname)

        result = [node for node in data.xpath("//con:soapui-project/con:restMockService/con:restMockAction", namespaces=nsmap)]

        for res in result:
            restMockActionList += str(etree.tostring(res, pretty_print=True))
    return restMockActionList

def makeBigMock(restMockActionList):
    return Template(
        '<?xml version="1.0" encoding="UTF-8"?>'+
        '<con:soapui-project id="${projectId}" activeEnvironment="Default" name="${name}" soapui-version="5.5.0" abortOnError="false" runType="SEQUENTIAL" resourceRoot="${"${projectDir}"}" xmlns:con="http://eviware.com/soapui/config"><con:settings/>'+
        '<con:restMockService id="${restMockServiceId}" port="${port}" path="/" host="${host}" name="mocks"><con:settings/><con:properties/>'+
        restMockActionList+
        '</con:restMockService><con:properties/><con:wssContainer/><con:oAuth2ProfileContainer/><con:oAuth1ProfileContainer/><con:sensitiveInformation/></con:soapui-project>'
    ).render(
        projectId="doesnt-matter",
        name="{}".format(args.name),
        host="localhost",
        port=7070,
        restMockServiceId="doesnt-really-matter"
    )

def main():
    restMockActionList = getRestMockActionBigString()
    tmplOut = makeBigMock(restMockActionList)

    f = open(projectDir+"soapui-mock/{}.xml".format(args.name), "w")
    f.write(tmplOut)
    f.close()

main()