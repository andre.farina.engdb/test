#!/bin/bash

echo "Creating Kong route for URI:[$URL] Release:[$RELEASE_NAME]\n"
KONG_SVC=$RELEASE_NAME-$WORKER_NAMESPACE

curl -X PUT \
  --url "$KONG_HOST/services/$KONG_SVC" \
  --data "url=http://$RELEASE_NAME.$WORKER_NAMESPACE$URL" \
  | grep -o '{.*}' | jq '.id' | sed 's/"//g' > svc.id

SVC_ID=`cat svc.id`

rm svc.id

curl -X PUT \
  --url "$KONG_HOST/routes/$KONG_SVC" \
  --data "paths[]=$URL" \
  --data "service.id=$SVC_ID"