image: alpine:latest

stages:
  - build
  - deploy

build_dev:
  stage: build
  image:
    name: andref5/kaniko-git  
    entrypoint: [""]
  script:
    - kaniko_docker_auth
    - kaniko_docker_build

deploy_dev:
  stage: deploy
  image: devth/helm
  script:
    - export WORKER_NAMESPACE=dev
    - gke_auth
    - deploy_mock
    - deploy

deploy_uat:
  stage: deploy
  image: devth/helm
  script:
    - export WORKER_NAMESPACE=uat
    - gke_auth pmid-uat southamerica-east1-a tim-pmid-fqa
    - deploy
  when: manual
  only:
    refs:
      - master

# ---------------------------------------------------------------------------

.bash_funcs: &bash_funcs |
  # Bash Script variables and functions

  function clone_ci_cd() {
    if [ ! -d "$CI_PROJECT_DIR/../ci-cd" ] 
    then
        git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/engdb/digilibs/ci-cd.git $CI_PROJECT_DIR/../ci-cd
    fi
  }

  function kaniko_docker_auth() {
    echo "${GCP_REGISTRY_KEY}" > /kaniko/.docker/key.json
    export GOOGLE_APPLICATION_CREDENTIALS=/kaniko/.docker/key.json
  }

  function ci_project_image_tag() {
    export CI_PROJECT_IMAGE_TAG=$CI_PROJECT_REPOSITORY:$CI_PROJECT_VERSION
  }

  function kaniko_docker_build() {
    clone_ci_cd
    ci_project_image_tag
    echo "Building worker docker image $CI_PROJECT_IMAGE_TAG - parent $WORKER_IMAGE:$WORKER_TAG "
    /kaniko/executor --context $CI_PROJECT_DIR \
                    --dockerfile $CI_PROJECT_DIR/../ci-cd/worker-z/Dockerfile \
                    --build-arg WORKER_IMAGE=${WORKER_IMAGE} \
                    --build-arg WORKER_TAG=${WORKER_TAG} \
                    --destination $CI_PROJECT_IMAGE_TAG
  }

  function deploy_mock() {
    clone_ci_cd
    export folder_mock=`ls $CI_PROJECT_DIR | grep mock`
    if [ ! -z "$folder_mock" ]; then
      echo "Instaling mocks"
      kubectl create configmap $folder_mock-$RELEASE_NAME --namespace=mock --from-file=$CI_PROJECT_DIR/$folder_mock -o yaml --dry-run | kubectl apply -f -
      helm_cmd="helm upgrade --install --recreate-pods --namespace mock \
          --set name=$RELEASE_NAME-mock \
          --set release=$RELEASE_NAME \
          --namespace=mock \
          --force --wait \
          $RELEASE_NAME-mock \
          $CI_PROJECT_DIR/../ci-cd/worker-z/dev/k8s/$folder_mock"
      echo $helm_cmd
      eval $helm_cmd
    fi
  }

  function kong_route() {
    if [ "$WORKER_SERVICE" = true ] ; then
      CFG=$CI_PROJECT_DIR/resources/application.*
      echo "Creating kong route form $CFG"
      cat $CFG
      URL=`[ -f $CFG ] && cat $CFG | grep "service.uri=" | awk -F '=' '{print $2}' | awk -F '{' '{print $1}' | tr -d '[:space:]'`

      if [ ! -z "$URL" ]
      then
        echo "URL=[$URL]"
        KONG_HOST=http://kong-kong-admin:8001
        kubectl run k-$RELEASE_NAME --attach --rm --namespace default \
        --restart=Never --image=andref5/kong-route \
          --env="WORKER_NAMESPACE=$WORKER_NAMESPACE" \
          --env="RELEASE_NAME=$RELEASE_NAME" \
          --env="KONG_HOST=$KONG_HOST" \
          --env="URL=$URL"
      fi
    fi
  }

  function deploy() {
    clone_ci_cd
    echo "Helm upgrade zeebe worker - Image $CI_PROJECT_IMAGE_TAG - GKE_CLUSTER $GKE_CLUSTER_NAME - NameSpace $WORKER_NAMESPACE"
    common=COMMON_ENV_$WORKER_NAMESPACE
    echo "${!common}" >> $CI_PROJECT_DIR/.env.$WORKER_NAMESPACE
    [ $WORKER_IMAGE == "start-z" ] && { export WORKER_SERVICE=true ;}; echo "WORKER_SERVICE=$WORKER_SERVICE"

    kubectl create configmap $RELEASE_NAME -n $WORKER_NAMESPACE --from-env-file=$CI_PROJECT_DIR/.env.$WORKER_NAMESPACE -o yaml --dry-run | kubectl apply -f -
    helm_cmd="helm upgrade --install --recreate-pods --namespace ${WORKER_NAMESPACE} \
      --set name=$RELEASE_NAME \
      --set runtime=${WORKER_IMAGE}-${WORKER_TAG} \
      --set image.repository=$CI_PROJECT_REPOSITORY \
      --set image.tag=$CI_PROJECT_VERSION \
      --set envFrom=$RELEASE_NAME \
      --set replicaCount=${WORKER_REPLICA_COUNT:-1} \
      --set java_opts=\"${JAVA_OPTS:-"-Xms512m -Xmx512m"}\" \
      --set service=${WORKER_SERVICE:-false} \
      --force --wait \
      $RELEASE_NAME-$WORKER_NAMESPACE \
      $CI_PROJECT_DIR/../ci-cd/worker-z/helm"
    echo $helm_cmd
    eval $helm_cmd
    kong_route
  }

  function gke_auth() {
    export GKE_CLUSTER_NAME=${1:-pmid-dev}
    export GKE_ZONE=${2:-southamerica-east1-a}
    export GKE_PROJECT=${3:-tim-pmid-dev}
    key=GCP_SVC_KEY_$WORKER_NAMESPACE
    key=${key//-/_}
    echo "GKE auth $key - $GKE_CLUSTER_NAME - $GKE_ZONE - $GKE_PROJECT"
    mkdir -p /etc/deploy
    echo "${!key}" > /etc/deploy/sa.json
    gcloud auth activate-service-account --key-file /etc/deploy/sa.json --project=${GKE_PROJECT}
    gcloud container clusters get-credentials ${GKE_CLUSTER_NAME} --zone ${GKE_ZONE} --project ${GKE_PROJECT}
    helm init --service-account tiller --wait --upgrade
  }

before_script:
  - export RELEASE_NAME=${CI_PROJECT_NAME:0:46}
  - export CI_PROJECT_REPOSITORY=$CI_REGISTRY/$RELEASE_NAME
  - export CI_PROJECT_VERSION=${CI_COMMIT_REF_NAME:0:2}-${CI_COMMIT_SHA:0:5}
  - export WORKER_TAG=latest
  - source $CI_PROJECT_DIR/.env
  - *bash_funcs