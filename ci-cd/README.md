# GitLab CI-CD

- Create .gitlab-ci.yml on project with the following content for "worker-z" or "start-z" PROJECT

```
include:
  - project: 'engdb/digilibs/ci-cd'
    file: 'worker-z/.gitlab-ci.yml'
```
  - At "worker-z" or "start-z" PROJECT
    - Create files ".env", ".env.dev", ".env.uat" and ".env.prd"
    - Define env var WORKER_IMAGE on file ".env"
      - sample WORKER_IMAGE=start-z
    - Default docker image tag is "latest", to special tag define env var WORKER_TAG on file ".env"
    - Define on files ".env.dev", ".env.uat" and ".env.prd" particular env vars to config application "-z" at environment.
      - [NOTE] - ci-cd will add to these files commons configs. Sample zeebe, redis, etc.


- For zeebe engdb "z" LIBS

```
include:
  - project: 'engdb/digilibs/ci-cd'
    file: 'worker-z/.gitlab-ci-parents.yml'
```


### For a faster pipeline, disable "Shared Runners" project > settings > CD/CI | Runners
