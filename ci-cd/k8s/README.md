# K8s configs
- Create GCP Service account with [Kubernetes Engine Developer] and [Storage Admin] Roles.

- Create JSON private key for created account.

- Run docker login with JSON key
	` ex. $ docker login -u _json_key --password-stdin https://gcr.io/tim-pmid-dev < tim-pmid-dev-8004907e3594.json `

- Test login trying pull some image
	` ex. $ docker pull gcr.io/tim-pmid-dev/mold:v1 `

- Create K8s secret with genereated docker auth ~/.docker/config.json named gcp-docker-config
    ` ex. $ kubectl create secret generic gcp-docker-config --from-file=.dockerconfigjson=$HOME/.docker/config.json --type=kubernetes.io/dockerconfigjson `

- Install Helm and apply rbac config (rbac-helm-config.yaml)
	` ex. $ kubectl create -f rbac-helm-config.yaml `
	` ex. $ helm init --service-account tiller --wait --upgrade `

- Install Gitlab Runner via helm
	` edit "runnerRegistrationToken" at gitlabci/digilibs-values.yaml`
	` ex. $ helm repo add gitlab https://charts.gitlab.io `
	` ex. $ helm install --name gitlab-runner --namespace ci-cd --values gitlabci/digilibs-values.yaml gitlab/gitlab-runner`